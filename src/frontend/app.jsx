let React = require("react");
require("babel-core/register");
require("babel-polyfill");
//const bcrypt = require('bcrypt');
import ReactDOM from "react-dom";

let user = null;

class Main extends React.Component {
  render() {
    if (user == null) {
      return <LoginScreen />;
    } else {
      return <UserLoader />;
    }
  }
}

class LoginScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = { name: "", password: "" };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.cacheName = this.cacheName.bind(this);
    this.cachePassword = this.cachePassword.bind(this);
  }
  render() {
    return (
      <div className="container">
        <div className="d-flex justify-content-center h-100">
          <div className="card">
            <div className="card-header">
              <h1>Click the button!</h1>
              <h3>Sign In</h3>
            </div>
            <div className="card-body">
              <form>
                <div className="input-group form-group">
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <i className="fas fa-user"></i>
                    </span>
                  </div>
                  <input
                    type="text"
                    autoFocus
                    value={this.state.name}
                    onChange={this.cacheName}
                    className="form-control"
                    placeholder="username"
                  />
                </div>
                <div className="input-group form-group">
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <i className="fas fa-key"></i>
                    </span>
                  </div>
                  <input
                    type="password"
                    onChange={this.cachePassword}
                    className="form-control"
                    placeholder="password"
                  />
                </div>
                <div className="row align-items-center remember"></div>
                <div className="form-group">
                  <button
                    type="submit"
                    onClick={this.handleSubmit}
                    value="Login"
                    className="btn float-right login_btn"
                  >
                    Login
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }

  handleSubmit(event) {
    event.preventDefault();
    let params = { name: this.state.name, password: this.state.password };
    let userInput = { name: this.state.name, password: this.state.password };

    let userExists = api("post", "users", params);
    userExists.then(function(response) {
      let userId = response.id;

      //existing user
      if (userExists.name == undefined) {
        let params = { password: userInput.password, clicks: 0 };
        let checkpassword = api("post", "users/" + userId, params);
        checkpassword.then(function(response) {
          if (response.valid == "true") {
            user = { id: userId, name: userInput.name };
            ReactDOM.render(<UserLoader />, document.getElementById("root"));
          } else {
            alert("Inalid password!");
          }
        });
      } //new user
      else {
        user = { id: userId, name: userInput.name, clicks: 0 };
        ReactDOM.render(<UserLoader />, document.getElementById("root"));
      }
    });
  }
  cacheName(e) {
    this.setState({ name: e.target.value });
  }
  cachePassword(e) {
    this.setState({ password: e.target.value });
  }
}

let users = [];
function UserLoader() {
  let userPromise = api("get", "");
  userPromise.then(function(result) {
    users = result;
    for (let i = 0; i < users.length; i++) {
      if (users[i].userId == user.id) {
        user.clicks = users[i].userClicks;
      }
    }
    ReactDOM.render(<Clicker />, document.getElementById("root"));
  });

  return <p>Loading...</p>;
}

class Clicker extends React.Component {
  constructor(props) {
    super(props);
    this.state = { clicks: user.clicks, counter: 0 };
    this.handleClick = this.handleClick.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
  }
  render() {
    return (
      <div className="container">
        <div className="d-flex justify-content-center">
          <div className="card">
            <div className="card-header">
              <h1>Click the button!</h1>
              <h4>My clicks: {this.state.clicks}</h4>
              <button
                className="btn btn-primary btn-lg btn-block"
                onClick={this.handleClick}
              >
                CLICK!
              </button>
            </div>
            <div className="card-body">
              <UserCards></UserCards>
            </div>
            <div className="card-footer">
              <div className="row">
                <div className="col-8">
                  <p>Logged in as {user.name}</p>
                </div>
                <div className="col-4">
                  <button
                    className="btn btn-warning"
                    onClick={this.handleLogout}
                  >
                    Logout
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
  handleClick(e) {
    let clicks = this.state.clicks + 1;
    this.setState({ clicks: clicks });

    updateClicks(user.id, clicks);
  }
  handleLogout(e) {
    user = null;
    loadMain();
  }
}

function UserCards(props) {
  const userItems = users.map(user => (
    <UserCard
      name={user.username}
      clicks={user.userClicks}
      key={user.key}
    ></UserCard>
  ));
  return (
    <div>
      <h4>Highscores</h4>
      <ul list-group>{userItems}</ul>
    </div>
  );
}

function UserCard(props) {
  return (
    <li className="list-group-item">
      Username: {props.name} - clicks: {props.clicks}
    </li>
  );
}

async function updateClicks(id, clicks) {
  let body = { clicks: clicks };
  api("put", "users/" + id, body);
}

async function api(method, path, body) {
  let opts = { method };
  if (body != null) {
    opts.headers = { "Content-Type": "application/json" };
    opts.body = JSON.stringify(body);
  }
  let response = await fetch("http://localhost:3000/" + path, opts);
  let data = await response.json();

  if (data.error) {
    throw new Error(data.error);
  }

  //React.refresh();
  return data;
}

function loadMain() {
  ReactDOM.render(<Main />, document.getElementById("root"));
}
loadMain();
