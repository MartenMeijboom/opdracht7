const express = require("express");
var bodyParser = require("body-parser");
const bcrypt = require("bcrypt");
const fs = require("fs");
const Database = require("better-sqlite3");
const db = new Database("./src/data/sqlite3.db");
const cors = require('cors')
db.exec(fs.readFileSync("./src/data/schema.sql").toString());
const app = express();
app.use(cors());

const router = express.Router();
module.exports = router;

let users = [];

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/", function(req, rsp) {
  rsp.setHeader('Access-Control-Allow-Origin', '*');
  rsp.json(db.prepare("select * from overview order by userClicks desc").all());
});

app.post("/users", function(req, rsp) {
  rsp.setHeader('Access-Control-Allow-Origin', '*');
  let name = req.body.name;
  let password = req.body.password;

  if (typeof name !== "string") {
    return rsp.status(400).json({ error: "Invalid name" });
  }
  if (typeof password !== "string") {
    return rsp.status(400).json({ error: "Invalid password" });
  }

  try{
    let userId = userExists(name);
    return rsp.status(200).json({ id: userId });
  }
  catch(error){
    console.log("Creating user " + name + "...");
  }
  let salt = bcrypt.genSaltSync();
  password = bcrypt.hashSync(password, salt);

  let info = db
    .prepare("insert into users(name, hash) values(?, ?)")
    .run(name, password);
  let id = info.lastInsertRowid;

  db
    .prepare("insert into clicks(userid, clicks) values(?, ?)")
    .run(id, 0);

    return rsp.status(201).json({ id, name });
});

app.post("/users/:userId", function(req, rsp) {
  rsp.setHeader('Access-Control-Allow-Origin', '*');
  if (correctPassword(req)) {
    return rsp.status(202).json({ valid: "true" });
  } else {
    return rsp.status(403).json({ valid: "false" });
  }
});

app.put("/users/:userId", function(req, rsp){
  rsp.setHeader('Access-Control-Allow-Origin', '*');

  let id = req.params.userId;
  let clicks = req.body.clicks;

  db
  .prepare("update clicks set clicks = ? where userid = ?")
  .run(clicks, id);

  return rsp.status(202).json({message: "updated"});
})

function userExists(name){
  let info = db.prepare("select id from users where name = ?").get(name);
  let id = info.id;
  return id;
}

function correctPassword(req){
  let id = req.params.userId;
  let password = req.body.password;

  let info = db.prepare("select hash from users where id = ?").get(id);
  let hash = info.hash;

  return bcrypt.compareSync(password, hash);
}

app.listen(3000);
