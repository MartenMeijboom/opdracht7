create table if not exists users(
  id integer primary key,
  name text not null,
  hash text not null
);

insert or replace into users(id,name, hash) values(1, "joris", "$2b$10$CH.zTXr2Xs2kDSHzFYQz2uKB.0fH5zle6HkkzzWvLVhPKo7sTqK.K");
insert or replace into users(id,name, hash) values(2, "marten", "$2b$10$CH.zTXr2Xs2kDSHzFYQz2uKB.0fH5zle6HkkzzWvLVhPKo7sTqK.K");

create table if not exists clicks(
  id integer primary key,
  userid integer,
  clicks integer
);

insert or replace into clicks(id, userid, clicks) values(1, 1, 11);
insert or replace into clicks(id, userid, clicks) values(2, 2, 5);

create view if not exists overview as
select user.id as userId,
user.name as username,
click.clicks as userClicks
from users user
left join clicks click on user.id = click.userid;