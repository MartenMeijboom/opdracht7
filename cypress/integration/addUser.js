describe('add user', function () {
    it('sings in as a new user.', function() {
        cy.visit('localhost:1234')

        cy.get(':nth-child(1) > .form-control').click().type('user')
        cy.get(':nth-child(2) > .form-control').click().type('test')

        cy.get('.btn').click()
    })
})