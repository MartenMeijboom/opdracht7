describe('clicking', function () {
    it('add fifty clicks.', function() {
        cy.visit('localhost:1234')

        cy.get(':nth-child(1) > .form-control').click().type('marten')
        cy.get(':nth-child(2) > .form-control').click().type('hash')

        cy.get('.btn').click()

        for (let index = 0; index < 50; index++) {
            cy.get('.card-header > .btn').click()
        }
    })
})