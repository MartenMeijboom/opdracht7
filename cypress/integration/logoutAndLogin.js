describe('login and logout', function () {
    it('sings out and than in.', function() {
        cy.visit('localhost:1234')

        cy.get(':nth-child(1) > .form-control').click().type('joris')
        cy.get(':nth-child(2) > .form-control').click().type('hash')

        cy.get('.btn').click()

        cy.contains('Logout').click()
    })
})